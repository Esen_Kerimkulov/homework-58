import React, { Component } from 'react';
import './App.css';
import ModalWindow from "./containers/ModalWindow/ModalWindow";

class App extends Component {
    render() {
        return (
            <ModalWindow />
        );
    }
}

export default App;
