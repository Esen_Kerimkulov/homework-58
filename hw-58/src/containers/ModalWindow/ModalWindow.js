import React, {Component, Fragment} from 'react';

import Modal from '../../components/UI/Modal/Modal';
import "./ModalWindow.css";


class ModalWindow extends Component {

    state = {
        ModalShow: false
    };

    modallShow = () => {
        this.setState({ModalShow: true});
    };

    modalClosed = () => {
        this.setState({ModalShow: false});
    };
    render() {
        return (
            <Fragment>
                <Modal
                    show={this.state.ModalShow}
                    close={this.modalClosed}
                    title="Some kinda modal title"
                >

                    <p>This is modal content</p>
                    <button className="btn" onClick={this.modalClosed}>Close</button>
                </Modal>
                <button className="butn" onClick={this.modallShow}>Modal Show</button>
            </Fragment>
        );
    }
}
export default ModalWindow;