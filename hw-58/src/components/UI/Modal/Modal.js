import React, {Fragment} from 'react';

import './Modal.css';

const Modal = props => {
    return (
        <Fragment>
            <div
                className="Modal"
                style={{
                    transform: props.show ? 'translateY(0)': 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}
            >
                <h3>{props.title}</h3>
                <hr/>
                {props.children}
            </div>

        </Fragment>
    );
};

export default Modal;